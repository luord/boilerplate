.DEFAULT_GOAL = .venv

PATH ::= .venv/bin:${PATH}

.venv: pyproject.toml package.json
	python3 -m venv --system-site-packages .venv
	.venv/bin/pip install poetry poethepoet nodeenv
	command -v node || nodeenv --force .venv
	poetry install
	npx npm install
	touch .venv

lint: | .venv
	npm run lint
	poe lint

test: | .venv
	npm run test
	poe test

debug: | .venv
	npm run debug
	poe test --pdb

watch: | .venv
	npm run watch

serve: | .venv
	poe serve

dist: src/spa | .venv
	npm run build
	touch dist

.PHONY: serve watch test lint debug
