from server import create_app


def test_root():
    client = create_app().test_client()

    res = client.get("/")

    assert b"hello from server" in res.get_data()
