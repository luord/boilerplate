import {html} from 'htm/preact'
import {test} from 'tape'
import {cleanup, render} from '@testing-library/preact'

import {App} from '@spa/app'

test('App', t => {
  const {container} = render(html`<${App} />`)
  t.is(container.textContent, 'hello from spa')

  t.end()
  cleanup()
})
