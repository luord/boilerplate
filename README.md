# Boilerplate for a web application

The back-end is a Flask based Python application.

The front-end, if needed, is a Preact based TypeScript application.

## Using

A `Makefile` is provided with common tasks. Note the `.venv` target for setting up.

### Running/Building

`make serve` runs the flask application in port `8000`. It watches the `src/server`, `src/templates` and `dist` folders.

`make watch` transpiles and bundles the preact application without compressing. It watches the `src/spa` folder and outputs to `dist`.

`make dist` transpiles and bundles the preact application.

### Testing

`make test` runs the tests for both back-end and front-end.

`make debug` runs the tests with debugging features enabled.

### Linting

`make lint` runs the linters.
