from os import getcwd
from pathlib import Path

from flask import Flask

from server.root import bp


INIT_PARAMS = {
    "root_path": getcwd(),
    "template_folder": str(Path("src", "templates")),
    "static_folder": str(Path("dist"))
}


def create_app(init_params=INIT_PARAMS):
    app = Flask(__name__, **init_params)

    app.register_blueprint(bp)

    return app
