from flask import Blueprint, render_template


bp = Blueprint("root", __name__)


@bp.route('/')
def root():
    return render_template("root.j2")
